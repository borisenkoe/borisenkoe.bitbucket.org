(function () {
    PlaybackUtils = {
        PlaybackTimer: function() {
            var timer = null;
            var isRunning = false;
            return {
                start: function(action, interval) {
                    if(timer !== null) {
                        clearInterval(timer);
                    }
                    timer = setInterval(action, interval);
                    isRunning = true;
                    $('.play').toggleClass('play pause');
                },
                stop: function() {
                    clearInterval(timer);
                    isRunning = false;
                    $('.pause').toggleClass('play pause'); // TODO:
                },
                isRunning: function() {
                    return isRunning;
                }
            }
        },
        toDateInputFormat: function (value) {
            var dd = value.getDate(),
                mm = value.getMonth() + 1,
                yy = value.getFullYear(),
                hh = value.getHours(),
                mi = value.getMinutes(),
                ss = value.getSeconds();

            dd = dd < 10 ? '0' + dd : dd;
            mm = mm < 10 ? '0' + mm : mm;
            hh = hh < 10 ? '0' + hh : hh;
            mi = mi < 10 ? '0' + mi : mi;
            ss = ss < 10 ? '0' + ss : ss;

            return yy + "-" + mm + "-" + dd + "T" + hh + ":" + mi + ":" + ss;
        },
        getUTCDateFromDateInput: function(dateInput) {
            var date = new Date(dateInput);
            var actualDate = new Date(date.setMinutes(date.getMinutes() + new Date().getTimezoneOffset()));
            return actualDate.toISOString();
        },
        changeColor: function(imgObject, imgColor) {
            // create hidden canvas
            var canvas = document.createElement("canvas");
            canvas.width = 40;
            canvas.height = 40;

            var ctx = canvas.getContext("2d");
            ctx.drawImage(imgObject, 0, 0, 40, 40);

            var map = ctx.getImageData(0, 0, 40, 40);
            var imdata = map.data;

            // convert image to grayscale first
            var r, g, b, avg;
            for (var p = 0, len = imdata.length; p < len; p += 4) {
                r = imdata[p]
                g = imdata[p + 1];
                b = imdata[p + 2];

                avg = Math.floor((r + g + b) / 3);

                imdata[p] = imdata[p + 1] = imdata[p + 2] = avg;
            }

            ctx.putImageData(map, 0, 0);

            // overlay using source-atop to follow transparency
            ctx.globalCompositeOperation = "source-atop"
            ctx.globalAlpha = 0.3;
            ctx.fillStyle = imgColor;
            ctx.fillRect(0, 0, 40, 40);

            // replace image source with canvas data
            return canvas.toDataURL("image/png", 1);
        },
        heuristicDeviceIdToColorTransformator: function (deviceId, normal) {
            var factor = 4;

            var offset = Math.max(0,deviceId.length-3);

            var mostSignificant = parseInt(deviceId[deviceId.length-1], 16);
            var lessSignificant = parseInt(deviceId[Math.max(0,1-offset)], 16);

            var mainColorIndex = Math.floor(mostSignificant % 3);

            var resultColor = deviceId.split('');

            while(resultColor.length < 3) {
                resultColor.push('f');
            }
            
            for(col in resultColor) {
                var hex = resultColor[col];
                var dec = Math.floor(parseInt(hex, 16) / factor);

                resultColor[col] = dec.toString(16);
            }

            resultColor[mainColorIndex] = ((mostSignificant*lessSignificant)%16).toString(16);

            var result = resultColor.join('').substring(0,3);

            if(normal) {
                var normalResult = "#";
                for(var i=0;i<result.length;i++) {
                    normalResult += result[i] + result[i];
                }
                return normalResult;
            }

            return '#' + result;
        },
        changeIconColor: function () {
          var canvas  = document.createElement("canvas"), // shared instance
              context = canvas.getContext("2d");

          // only place the dimensions are hardcoded
          // everything else just references canvas.width/canvas.height
          canvas.width  = 200;
          canvas.height = 200;

          function hexToRgb(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
          }

          function colorize(hexColor) {
            var color = hexToRgb(hexColor);

            var imageData = context.getImageData(0, 0, canvas.width, canvas.height);
            var pixels    = imageData.data;

            for(i = 0, l = pixels.length ; i < l ; i += 4) {

              a = pixels[i + 3];
              if( a === 0 ) { continue; } // skip if pixel is transparent

              pixels[i] = color.r;
              pixels[i + 1] = color.g;
              pixels[i + 2] = color.b;
            }

            context.putImageData(imageData, 0, 0);
          }

          return function (iconElement, color, alpha) {
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.drawImage(iconElement, 0, 0, canvas.width, canvas.height);
            colorize(color, alpha);
            return canvas.toDataURL("image/png", 1);
          };
        }(),
        processDataForTable: function(device, trips) {

            var deviceTripInfo = {};

            function timeToString(time) {
                time = Math.floor(time / 1000);
                var seconds = time % 60;
                seconds = seconds < 10 ? '0' + seconds : seconds;
                time = Math.floor(time / 60);
                var minutes = time % 60;
                minutes = minutes < 10 ? '0' + minutes : minutes;
                time = Math.floor(time / 60);
                var hours = time % 24;
                hours = hours < 10 ? '0' + hours : hours;

                return hours + ':' + minutes + ':' + seconds;
            }

            function stringToTime(timeString, timeReference) {
                var time = new Date(timeReference);
                time.setHours(timeReference.getHours() + parseInt(timeString.split(':')[0]));
                time.setMinutes(timeReference.getMinutes() + parseInt(timeString.split(':')[1]));
                time.setSeconds(timeReference.getSeconds() + parseInt(timeString.split(':')[2].split('.')[0]));
                return time-timeReference;
            } 

            var timeReference = new Date();



            deviceTripInfo = {
                drivingDuration: 0,
                stopDuration: 0,
                averageSpeed: 0,
                numberOfTrips: 0
            };
                $.each(trips, function(k, trip) {

                    // TODO: check adding objects?
                    deviceTripInfo.drivingDuration += (stringToTime(trip.drivingDuration, timeReference));
                    deviceTripInfo.stopDuration += (stringToTime(trip.stopDuration, timeReference));
                    deviceTripInfo.averageSpeed += trip.averageSpeed;
                    deviceTripInfo.numberOfTrips++;
                });

            deviceTripInfo.averageSpeed /= deviceTripInfo.numberOfTrips;
            deviceTripInfo.averageSpeed = deviceTripInfo.averageSpeed.toFixed(1);
            deviceTripInfo.drivingDuration = timeToString(deviceTripInfo.drivingDuration);
            deviceTripInfo.stopDuration = timeToString(deviceTripInfo.stopDuration);

            return deviceTripInfo;
        },
        getFirstTripStart: function(date, selected, allTrips) {
            var timeReference = new Date(date);
            var minTime = new Date(timeReference.setHours(0,0,0,0)).toISOString();
            var maxTime = new Date(timeReference.setHours(23,59,59,999)).toISOString();
            var firstTripStart = new Date(timeReference.setHours(23,59,59,999)).toISOString();



            $.each(allTrips, function(k, trips) {
                if(!(trips instanceof Array)) trips = [trips];
                $.each(trips, function(k, trip) {
                    if(trip.start < firstTripStart && selected.indexOf(trip.device.id) !== -1)
                        firstTripStart = trip.start;
                });
            });

            if(firstTripStart > maxTime) firstTripStart = maxTime;
            if(firstTripStart < minTime) firstTripStart = minTime;
            return firstTripStart;
        }
    }
})();