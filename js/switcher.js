(function () {
    Switcher = function(layers) {
        return {
            switch: function(layers) {
                var index = 0;
                return function() {
                    return layers[(++index)%layers.length];
                }
            }(layers)
        };
    }
})();
